/**
 * Created by Max on 06.01.2018.
 */
class Machine {
    constructor() {
        this.isLaunched = false;
    };


    turnOn() {
        if (!this.isLaunched) {
            this.isLaunched = true;
            console.log("Запущен механихм")

        }
        else
            console.log("Механизм уже запущен")
    };

    turnOff() {
        if (this.isLaunched) {
            this.isLaunched = false;
            console.log("Механизм был заглушен")

        }
        else
            console.log("Механизм уже заглушен")
    }


}
class HomeAppliance extends Machine {
    constructor() {
        super();
        this.isConnect = false;
    }

    plugOn() {
        if (!this.isConnect) {
            this.isConnect = true;
            console.log("Прибор подключен к сети")

        }
        else
            console.log("Прибор уже подключен")
    };

    plugOff() {
        if (this.isConnect) {
            this.isConnect = false;
            this.isLaunched = false;
            console.log("Прибор был отключен от сети")

        }
        else
            console.log("Прибор уже отключен")
    }

    turnOn() {
        if (this.isConnect)
            super.turnOn();
        else console.log("Подключите к питанию")
    }


}
class WashingMachine extends HomeAppliance {
    constructor() {
        super();
        this.isRun = false;
    };

    run() {
        if (this.isConnect && this.isLaunched) {
            console.log("Машинка запущена");
            this.isRun = true;
        }

        else
            console.log("Проверьте питание и двигатель");
    }

    plugOff() {
        super.plugOff();
        this.isRun = false;
    };

    turnOff() {
        super.turnOff();
        this.isRun = false;
    }
}
class LightSource extends HomeAppliance {
    constructor() {
        super();
        this.lightLevel = 0;
    }

    setLevel(level) {

        this.lightLevel = Math.max(1,Math.min(100,level));
        console.log("Яркость установлена на ", this.lightLevel)

    }

}
class AutoVehicle extends Machine {
    constructor() {
        super();
        this.xPosition = 0;
        this.yPosition = 0;
    }

    setPosition(x, y) {
        this.xPosition = x;
        this.yPosition = y;
        console.log("Позиция " + x + " " + y);
    }
}
class Car extends AutoVehicle {
    constructor() {
        super();
        this.speed = 10;
    };

    setSpeed(speed) {
        if (speed > 0) {
            this.speed = speed;
            console.log("заданая в скорость ", speed, " можно отправляться")
        }
        else
            throw Error("Скорость задана некорректно, сохранена исходная скорость в ", this.speed);
    };




    run (x, y){
        console.log(`Начало движения из(${this.xPosition}, ${this.yPosition}) в(${x},${y}) со скоростью: ${this.speed}`);
        this.directionX = x - this.xPosition < 0 ? -1 : 1;
        this.directionY = y - this.yPosition < 0 ? -1 : 1;
        let limitUpValue = (current, end, v)=>{
            return (end - current) * v >= 0 ? current : end;
        };
        let changePosition = ()=>{
            let newX = this.xPosition + this.speed * this.directionX;
            let newY = this.yPosition + this.speed * this.directionY;
            this.setPosition(limitUpValue(newX, x, this.directionX), limitUpValue(newY, y, this.directionY));
            console.log(`Автомобиль в точке:(${this.xPosition},${this.yPosition})`);
        };
        if(this.isLaunched){
            let runTime = setInterval(()=>{
                changePosition();
                if(this.xPosition === x && this.yPosition === y){
                    console.log("Автомобиль в пункте назначения");
                    clearTimeout(runTime);
                }
            }, 1000);
        }
    };


}